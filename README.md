# gif.lib
This application is used in the Spring with Hibernate Treehouse course. The app was originally built in the Spring Basics course, and this app extends functionality in the following ways:
 
- Adds persisted data, using an in-memory H2 database
- Manages data with Hibernate

What this app does:

- Serves dynamic web content according to URL, including index and detail pages
- Includes database connectivity, where GIF data is stored
- Allows a user to perform CRUD (create, read, update, delete) operations on GIF data
- Uses an underlying data source (static, Java data)
- Serves static assets, such as images, fonts, CSS, and JS

What this app does **NOT** do:

- Implement user authentication
- Persist data after application terminates