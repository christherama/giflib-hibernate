package com.teamtreehouse.giflib.service;

import com.teamtreehouse.giflib.model.Gif;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface GifService {
    Gif findById(Long id);
    List<Gif> findAll();
    void save(Gif gif, MultipartFile file);
    void update(Gif gif, MultipartFile file);
    List<Gif> getFavorites();
    void delete(Gif gif);
    void toggleFavorite(Gif gif);
    List<Gif> findByDescription(String q);
}
