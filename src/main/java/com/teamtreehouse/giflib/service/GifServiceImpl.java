package com.teamtreehouse.giflib.service;

import com.teamtreehouse.giflib.dao.GifDao;
import com.teamtreehouse.giflib.model.Gif;
import org.hashids.Hashids;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
public class GifServiceImpl implements GifService {
    @Autowired
    private GifDao gifDao;

    @Autowired
    private Hashids hashids;

    @Override
    public Gif findById(Long id) {
        return gifDao.findById(id);
    }

    @Override
    public List<Gif> findAll() {
        return gifDao.findAll();
    }

    @Override
    public void save(Gif gif, MultipartFile file) {
        try {
            gif.setBytes(file.getBytes());
        } catch (IOException e) {

        }
        gifDao.save(gif);
        gif.setHash(hashids.encode(gif.getId()));
        gifDao.save(gif);
    }

    @Override
    public void update(Gif gif, MultipartFile file) {
        if(file != null && !file.isEmpty()) {
            try {
                gif.setBytes(file.getBytes());
            } catch (IOException e) {

            }
        } else {
            Gif oldGif = gifDao.findById(gif.getId());
            gif.setBytes(oldGif.getBytes());
        }

        gifDao.save(gif);
    }

    @Override
    public List<Gif> getFavorites() {
        return gifDao.getFavorites();
    }

    @Override
    public void delete(Gif gif) {
        gifDao.delete(gif);
    }

    @Override
    public void toggleFavorite(Gif gif) {
        gif.setFavorite(!gif.isFavorite());
        gifDao.save(gif);
    }

    @Override
    public List<Gif> findByDescription(String q) {
        return gifDao.findByDescription(q.toLowerCase());
    }
}
