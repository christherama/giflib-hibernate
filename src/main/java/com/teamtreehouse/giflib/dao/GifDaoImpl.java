package com.teamtreehouse.giflib.dao;

import com.teamtreehouse.giflib.model.Gif;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GifDaoImpl extends HibernateDao<Gif> implements GifDao {

    @Override
    public Gif findById(Long id) {
        Session session = getSessionFactory().openSession();
        Gif gif = session.get(Gif.class,id);
        session.close();
        return gif;
    }

    @Override
    public List<Gif> findAll() {
        return findAll(Gif.class);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Gif> findByCategoryId(Long id) {
        Session session = getSessionFactory().openSession();
        List<Gif> categoryGifs = session.createCriteria(Gif.class).add(Restrictions.eq("category.id",id)).list();
        session.close();
        return categoryGifs;
    }

    @Override
    public Gif findByName(String name) {
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Gif> getFavorites() {
        Session session = getSessionFactory().openSession();
        List<Gif> favorites = session.createCriteria(Gif.class).add(Restrictions.eq("favorite",true)).list();
        session.close();
        return favorites;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Gif> findByDescription(String q) {
        Session session = getSessionFactory().openSession();
        List<Gif> searchResults = session.createCriteria(Gif.class).add(Restrictions.like("description","%" + q + "%").ignoreCase()).list();
        session.close();
        return searchResults;
    }


}
