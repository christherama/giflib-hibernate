package com.teamtreehouse.giflib.dao;

import com.teamtreehouse.giflib.model.Category;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryDaoImpl extends HibernateDao<Category> implements CategoryDao {

    @Override
    public Category findById(Long id) {
        Session session = getSessionFactory().openSession();
        Category category = (Category)session.get(Category.class,id);
        Hibernate.initialize(category.getGifs());
        session.close();
        return category;
    }

    @Override
    public List<Category> findAll() {
        return findAll(Category.class);
    }
}