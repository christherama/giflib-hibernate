package com.teamtreehouse.giflib.dao;

import com.teamtreehouse.giflib.model.Gif;

import java.util.List;

public interface GifDao {
    Gif findById(Long id);
    List<Gif> findByCategoryId(Long id);
    List<Gif> findAll();
    Gif findByName(String name);
    List<Gif> getFavorites();
    void save(Gif gif);
    void delete(Gif gif);
    List<Gif> findByDescription(String q);
}