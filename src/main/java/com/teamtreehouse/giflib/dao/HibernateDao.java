package com.teamtreehouse.giflib.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public abstract class HibernateDao<E> {
    @Autowired
    private SessionFactory sessionFactory;

    public void save(E entity) {
        try {
            Session session = sessionFactory.openSession();
            session.beginTransaction();
            session.saveOrUpdate(entity);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException e) {
            System.err.printf("Save failed: %s%n", e);
        }
    }

    public void delete(E entity) {
        try {
            Session session = sessionFactory.openSession();
            session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
            session.close();
        } catch (HibernateException e) {
            System.err.printf("Delete failed: %s%n", e);
        }
    }

    @SuppressWarnings("unchecked")
    public List<E> findAll(Class clazz) {
        Session session = sessionFactory.openSession();
        List<E> all = session.createCriteria(clazz).list();
        session.close();
        return all;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public abstract List<E> findAll();
}
