package com.teamtreehouse.giflib.web.controller;

import com.teamtreehouse.giflib.model.Gif;
import com.teamtreehouse.giflib.service.CategoryService;
import com.teamtreehouse.giflib.service.GifService;
import com.teamtreehouse.giflib.validator.GifValidator;
import com.teamtreehouse.giflib.web.FlashMessage;
import com.teamtreehouse.giflib.web.FlashMessage.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
public class GifController {
    @Autowired
    private GifService gifService;

    @Autowired
    private CategoryService categoryService;

    @InitBinder("gif")
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new GifValidator());
    }

    // Home page - index of all GIFs
    @RequestMapping("/")
    public String listGifs(Model model) {
        List<Gif> gifs = gifService.findAll();
        model.addAttribute("gifs", gifs);
        return "gif/index";
    }

    // Single GIF page
    @RequestMapping("/gifs/{id}")
    public String gifDetails(@PathVariable Long id, Model model) {
        Gif gif = gifService.findById(id);
        model.addAttribute("gif", gif);
        return "gif/details";
    }

    // GIF image data
    @RequestMapping("/gifs/{id}.gif")
    @ResponseBody
    public byte[] gifImage(@PathVariable Long id) {
        return gifService.findById(id).getBytes();
    }

    // Favorites - index of all GIFs marked favorite
    @RequestMapping("/favorites")
    public String favorites(Model model) {
        List<Gif> faves = gifService.getFavorites();
        model.addAttribute("gifs",faves);
        model.addAttribute("username","Chris Ramacciotti");
        return "gif/favorites";
    }

    // Upload a new GIF
    @RequestMapping(value = "/gifs", method = RequestMethod.POST)
    public String addGif(@Valid Gif gif, BindingResult result, RedirectAttributes redirectAttributes) {
        if(result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.gif",result);
            redirectAttributes.addFlashAttribute("gif",gif);
            return "redirect:/upload";
        }

        gifService.save(gif, gif.getFile());
        redirectAttributes.addFlashAttribute("flash",new FlashMessage("GIF added!", Status.SUCCESS));
        return String.format("redirect:/gifs/%s",gif.getId());
    }

    // Form for uploading a new GIF
    @RequestMapping("/upload")
    public String formNewGif(Model model) {
        if(!model.containsAttribute("gif")) {
            model.addAttribute("gif",new Gif());
        }
        model.addAttribute("categories",categoryService.findAll());
        String action = "/gifs";
        model.addAttribute("heading","Upload");
        model.addAttribute("action",action);
        model.addAttribute("submit","Upload");
        return "gif/form";
    }

    // Form for editing an existing GIF
    @RequestMapping(value = "/gifs/{id}/edit")
    public String formEditGif(@PathVariable Long id, Model model) {
        if(!model.containsAttribute("gif")) {
            model.addAttribute("gif",gifService.findById(id));
        }
        model.addAttribute("categories",categoryService.findAll());
        String action = String.format("/gifs/%s",id);
        model.addAttribute("heading","Edit");
        model.addAttribute("action",action);
        model.addAttribute("submit","Update");
        return "gif/form";
    }

    // Update an existing GIF
    @RequestMapping(value = "/gifs/{gifId}", method = RequestMethod.POST)
    public String updateGif(@Valid Gif gif, BindingResult result, RedirectAttributes redirectAttributes) {
        if(result.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.gif",result);
            redirectAttributes.addFlashAttribute("gif",gif);
            return String.format("redirect:/gifs/%s/edit",gif.getId());
        }
        gifService.update(gif,gif.getFile());
        redirectAttributes.addFlashAttribute("flash",new FlashMessage("GIF updated!", Status.SUCCESS));
        return String.format("redirect:/gifs/%s",gif.getId());
    }

    // Delete an existing GIF
    @RequestMapping(value = "/gifs/{id}/delete", method = RequestMethod.POST)
    public String deleteGif(@PathVariable Long id, RedirectAttributes redirectAttributes) {
        Gif gif = gifService.findById(id);
        gifService.delete(gif);
        redirectAttributes.addFlashAttribute("flash",new FlashMessage("GIF deleted!", Status.SUCCESS));
        return "redirect:/";
    }

    // Mark/unmark an existing GIF as a favorite
    @RequestMapping(value = "/gifs/{id}/favorite", method = RequestMethod.POST)
    public String toggleFavorite(@PathVariable Long id, HttpServletRequest request) {
        Gif gif = gifService.findById(id);
        gifService.toggleFavorite(gif);
        return String.format("redirect:%s",request.getHeader("referer"));
    }

    // Search results
    @RequestMapping("/search")
    public String searchResults(@RequestParam String q, Model model) {
        List<Gif> gifs = gifService.findByDescription(q);
        model.addAttribute("gifs",gifs);
        return "gif/index";
    }
}